# Fugitif 1991

Fugitif is an Amstrad CPC adventure game I developed with my cousin, Laurent Dieudonné, between 1986 and 1991.

![Fugitif Splash screen](screens/png/splash.png "Fugitif splash screen")

This game uses rasters to display images in mode 1 with more than 4 colors.

Thanks to Jean-Paul Renaud, who made the screens, and got the most of this trick.

Also a big thanks to Laurent Mollard, who wrote the nice intro music.

As I lost/threw away all the source code and development documents (!), I made some reverse engineering to release most part of the code.

You will find:
- dissasembled and commented code (as far as I can remember what's doing what!)
- game in DSK format (Chany version uses files; Nich version loads from sectors, as original game)
- solution
- Lankhor work (box and leaflet)
- original music sheet
- reviews of Fugitif
- screens in CPC format (thanks to Siko) and PNG (from cpcpower)

Enjoy!

Frédéric Mantegazza

_Note: I started to work on a new release of Fugitif, to address the main issue of the original game: at the time, I was unable to do anything else during the raster display. The new release will use demomakers tricks to display a hud below the image. This hud will non longer be icons-based, but rather a console, whith a syntax interpreter, as in many adventure games in this era (and something I originaly wanted to do). The gameplay will also be entirely revisited, with a lot of different actions, and no more sudden deaths nor soft locks! Last, the game will be translated in english, and probably spanish._

![Fugitif preview](preview_2024.png "Fugitif preview")
