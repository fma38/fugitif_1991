; Copyright (C) 1991 Frédéric Mantegazza
;                    Laurent Dieudonné
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

; Load Intro
#0600   LD   D,#03          1603       ..
#0602   LD   B,#2D          062D       .-
#0604   LD   C,#02          0E02       ..
#0606   LD   HL,#27E0       21E027     !.'
#0609   CALL #060F          CD0F06     ...
#060C   JP   #7DF0          C3F07D     ..}

; -----------------------------------------
; Load data from sectors
; HL=dest
; B=nb sectors
; D=track
; E=drive
; C=start sector
#060F   PUSH HL             E5         .
#0610   PUSH DE             D5         .
#0611   PUSH BC             C5         .
#0612   LD   C,#07          0E07       ..
#0614   CALL #B90F          CD0FB9     ...      ; select ROM 7 (AMSDOS)
#0617   POP  BC             C1         .
#0618   POP  DE             D1         .
#0619   POP  HL             E1         .
#061A   LD   E,#00          1E00       ..       ; force drive 0
#061C   CALL #C666          CD66C6     .f.      ; read sector (HL=buffer, E=drive, D=track, C=sector)
#061F   LD   A,C            79         y
#0620   INC  C              0C         .
#0621   CP   #08            FE08       ..       ; last sector of track?
#0623   JR   C,#0628        3803       8.
#0625   LD   C,#00          0E00       ..       ; if yes: reset num sector
#0627   INC  D              14         .        ; inc num track
#0628   INC  H              24         $        ; inc buffer
#0629   INC  H              24         $
#062A   DJNZ #061C          10F0       ..       ; loop
#062C   CALL #B903          CD03B9     ...      ; switch off ROM 7 (AMSDOS), and resume loader
#062F   RET                 C9         .
; -----------------------------------------

; Team name
#0630   LD   HL,(#5243)     2A4352     *CR
#0633   LD   E,C            59         Y
#0634   LD   D,E            53         S
#0635   LD   D,H            54         T
#0636   LD   B,C            41         A
#0637   LD   C,H            4C         L
#0638   JR   NZ,#065A       2020
#063A   LD   B,H            44         D
#063B   LD   D,D            52         R
#063C   LD   B,L            45         E
#063D   LD   B,C            41         A
#063E   LD   C,L            4D         M
#063F   LD   HL,(#5021)     2A2150     *!P

; -----------------------------------------
; Entry point
;
; Descramble loader
#0640   LD   HL,#0650       215006     !P.
#0643   LD   BC,#01B0       01B001     ...
#0646   LD   A,(HL)         7E         ~
#0647   XOR  #92            EE92       ..
#0649   LD   (HL),A         77         w
#064A   INC  HL             23         #
#064B   DEC  BC             0B         .
#064C   LD   A,B            78         x
#064D   OR   C              B1         .
#064E   JR   NZ,#0646       20F6        .

; Descrambled loader Entry point
#0650   LD   HL,#4000       210040     !.@
#0653   LD   DE,#4001       110140     ..@
#0656   LD   BC,#3FFF       01FF3F     ..?
#0659   LD   (HL),#00       3600       6.
#065B   LDIR                EDB0       ..       ; reset memory from #4000 to #7FFF
#065D   CALL #A000          CD00A0     ...      ; immediate return, as was patched with #C9 by the bootloader

; ??? Protection?
#0660   LD   B,E            43         C        ; B is #00
#0661   DEC  C              0D         .        ; C is now #FF
#0662   CP   #FF            FEFF       ..       ; A is #00, test if false
#0664   LD   H,#C9          26C9       &.
#0666   LD   (#0000),A      320000     2..      ; Patch
#0669   RET  Z              C8         .        ; never ret

; Load first screen
#066A   LD   D,#08          1608       ..       ; track
#066C   LD   C,#02          0E02       ..       ; sector
#066E   LD   B,#20          0620       .        ; nb sectors
#0670   LD   HL,#C000       2100C0     !..      ; dest
#0673   CALL #060F          CD0F06     ...

; Load game (from #8C00 -> #A5FF)
#0676   LD   D,#0B          160B       ..       ; track
#0678   LD   C,#07          0E07       ..       ; sector
#067A   LD   B,#0D          060D       ..       ; nb sectors
#067C   LD   HL,#8C00       21008C     !..      ; dest
#067F   CALL #060F          CD0F06     ...

; Select ROM 7 (AMSDOS)
#0682   LD   A,#07          3E07       >.
#0684   CALL #B90F          CD0FB9     ...      ; select ROM 7 (AMSDOS)

;
#0687   LD   A,(#C002)      3A02C0     :..      ; load value from screen -> #02
#068A   LD   (#A670),A      3270A6     2p.      ; store for the bootloader
#068D   PUSH AF             F5         .
#068E   CALL #B903          CD03B9     ...      ; switch off ROM sup
#0691   POP  AF             F1         .
#0692   CP   #02            FE02       ..
#0694   JR   Z,#069C        2806       (.       ; jump here
#0696   CP   #04            FE04       ..
#0698   JR   Z,#069C        2802       (.
#069A   JR   #06EE          1852       .R

; Write #C4 on GA -> select RAM bank #4 (?!?)
#069C   LD   A,#C4          3EC4       >.
#069E   CALL #07AF          CDAF07     ...

; Load game (from #8000 -> #8BFF)
#06A1   LD   D,#0D          160D       ..       ; track
#06A3   LD   C,#02          0E02       ..       ; sector
#06A5   LD   B,#06          0606       ..       ; nb sectors
#06A7   LD   HL,#8000       210080     !..      ; dest
#06AA   CALL #060F          CD0F06     ...

; Load ??? (from #3800 to #3BFF)
#06AD   LD   D,#00          1600       ..       ; track
#06AF   LD   C,#01          0E01       ..       ; sector
#06B1   LD   B,#02          0602       ..       ; nb sectors
#06B3   LD   HL,#3800       210038     !.8      ; dest
#06B6   CALL #060F          CD0F06     ...

; Load ???(from #3B00 to #62FF)
#06B9   LD   D,#01          1601       ..       ; track
#06BB   LD   C,#00          0E00       ..       ; sector
#06BD   LD   B,#14          0614       ..       ; nb sectors
#06BF   LD   HL,#3B00       21003B     !.;
#06C2   CALL #060F          CD0F06     ...

; Set charset table
#06C5   LD   DE,#0020       112000     . .      ; first char is space
#06C8   LD   HL,#1000       210010     !..      ; address of charset table
#06CB   CALL #BBAB          CDABBB     ...

; Load charset from disk (from #1000 -> #13FF)
#06CE   LD   D,#00          1600       ..       ; track
#06D0   LD   C,#06          0E06       ..       ; sector
#06D2   LD   B,#02          0602       ..       ; nb sectors
#06D4   LD   HL,#1000       210010     !..      ; dest
#06D7   CALL #060F          CD0F06     ...

; Copy memory from #1000 to #B300 (728 bytes)
#06DA   LD   HL,#1000       210010     !..
#06DD   LD   DE,#B300       1100B3     ...
#06E0   PUSH DE             D5         .
#06E1   LD   BC,#02D8       01D802     ...
#06E4   LDIR                EDB0       ..
#06E6   POP  DE             D1         .
#06E7   LD   (#B736),DE     ED5336B7   .S6.
#06EB   RET                 C9         .        ; return to bootloader (#0137)

;
#06EC   ADC  A,C            89         .
#06ED   RLCA                07         .

; Load (from #8000 -> #8BFF)
#06EE   LD   D,#0D          160D       ..       ; track
#06F0   LD   C,#08          0E08       ..       ; sector
#06F2   LD   B,#06          0606       ..       ; nb sectors
#06F4   LD   HL,#8000       210080     !..      ; dest
#06F7   CALL #060F          CD0F06     ...

; Set charset table
#06FA   LD   DE,#0020       112000     . .      ; first char is space
#06FD   LD   HL,#1000       210010     !..      ; address of charset table
#0700   CALL #BBAB          CDABBB     ...

; Load charset from disk (from #1000 -> #13FF)
#0703   LD   D,#00          1600       ..       ; track
#0705   LD   C,#06          0E06       ..       ; sector
#0707   LD   B,#02          0602       ..       ; nb sectors
#0709   LD   HL,#1000       210010     !..      ; dest
#070C   CALL #060F          CD0F06     ...

#070F   LD   DE,#B300       1100B3     ...
#0712   LD   A,(#A670)      3A70A6     :p.      ; value from #C002 (cf @ #0687) A=#01
#0715   CP   #01            FE01       ..
#0717   JR   Z,#071C        2803       (.       ; jump here
#0719   LD   DE,#AD00       1100AD     ...
#071C   PUSH DE             D5         .
#071D   LD   HL,#1000       210010     !..
#0720   LD   BC,#02D8       01D802     ...
#0723   LDIR                EDB0       ..       ; copy memory from #1000 to #AD00, #02D8 (728) long
#0725   POP  DE             D1         .
#0726   LD   (#B736),DE     ED5336B7   .S6.
#072A   LD   (#B296),DE     ED5396B2   .S..
#072E   LD   HL,#4000       210040     !.@
#0731   LD   DE,#4001       110140     ..@
#0734   LD   BC,#3FFF       01FF3F     ..?
#0737   LD   (HL),#00       3600       6.
#0739   LDIR                EDB0       ..       ; clear memory from #4000 to #7FFF
#073B   LD   HL,#0749       214907     !I.
#073E   LD   DE,#3000       110030     ..0
#0741   LD   BC,#0067       016700     .g.
#0744   LDIR                EDB0       ..       ; copy memory from #0749 to #3000 (103 bytes long)
#0746   JP   #3000          C30030     ..0

; Load ??? from #0300 to #06FF
#0749   LD   C,#07          0E07       ..
#074B   CALL #B90F          CD0FB9     ...      ; select ROM 7 (AMSDOS)
#074E   LD   D,#00          1600       ..       ; track
#0750   LD   C,#01          0E01       ..       ; sector
#0752   LD   B,#02          0602       ..       ; nb sectors
#0754   LD   HL,#0300       210003     !..      ; dest
#0757   LD   E,#00          1E00       ..
#0759   CALL #C666          CD66C6     .f.
#075C   LD   A,C            79         y
#075D   INC  C              0C         .
#075E   CP   #08            FE08       ..
#0760   JR   C,#0765        3803       8.
#0762   LD   C,#00          0E00       ..
#0764   INC  D              14         .
#0765   INC  H              24         $
#0766   INC  H              24         $
#0767   DJNZ #0759          10F0       ..

; Load ??? from #0600 to #21FF
#0769   LD   D,#01          1601       ..       ; track
#076B   LD   C,#00          0E00       ..       ; sector
#076D   LD   B,#14          0614       ..       ; nb sectors
#076F   LD   HL,#0600       210006     !..      ; dest
#0772   LD   E,#00          1E00       ..
#0774   CALL #C666          CD66C6     .f.
#0777   LD   A,C            79         y
#0778   INC  C              0C         .
#0779   CP   #08            FE08       ..
#077B   JR   C,#0780        3803       8.
#077D   LD   C,#00          0E00       ..
#077F   INC  D              14         .
#0780   INC  H              24         $
#0781   INC  H              24         $
#0782   DJNZ #0774          10F0       ..
#0784   CALL #B903          CD03B9     ...      ; switch off ROM 7 (AMSDOS)
#0787   RET                 C9         .

; unused
#0788   NOP                 00         .

; ???
#0789   LD   HL,#0000       210000     !..      ; (#0000) is #00 (patched @ #0666)
#078C   INC  (HL)           34         4
#078D   LD   A,#01          3E01       >.
#078F   RET  Z              C8         .        ; do not ret

; Set colors of pens 1, 2 & 3
#0790   LD   BC,#0202       010202     ...      ; color ?
#0793   CALL #BC32          CD32BC     .2.
#0796   LD   A,#02          3E02       >.
#0798   LD   BC,#0101       010101     ...      ; color ?
#079B   CALL #BC32          CD32BC     .2.
#079E   LD   A,#03          3E03       >.
#07A0   LD   BC,#1414       011414     ...      ; color ?
#07A3   CALL #BC32          CD32BC     .2.

; Load icon panel
#07A6   CALL #8000          CD0080     ...

; Start Game
#07A9   LD   SP,#BFF8       31F8BF     1..
#07AC   JP   #8C00          C3008C     ...
; -----------------------------------------

; -----------------------------------------
; Out A on port #7F (GA)
#07AF   DI                  F3         .
#07B0   EXX                 D9         .
#07B1   OUT  (C),A          ED79       .y
#07B3   EXX                 D9         .
#07B4   EI                  FB         .
#07B5   RET                 C9         .
; -----------------------------------------

; ??
#07B6   SBC  A,(HL)         9E         .
#07B7   INC  HL             23         #
#07B8   LD   B,D            42         B
#07B9   ADD  HL,SP          39         9
#07BA   JR   NC,#07EF       3033       03
#07BC   DEC  C              0D         .
#07BD   ADC  A,H            8C         .
#07BE   LD   B,C            41         A
#07BF   LD   B,(HL)         46         F
#07C0   DEC  C              0D         .
#07C1   ADC  A,D            8A         .
#07C2   INC  HL             23         #
#07C3   JR   NC,#07F7       3032       02
#07C5   DEC  C              0D         .
#07C6   SBC  A,L            9D         .
#07C7   LD   E,D            5A         Z
#07C8   INC  L              2C         ,
#07C9   LD   D,(HL)         56         V
#07CA   LD   (HL),#31       3631       61
#07CC   LD   (#0D38),A      32380D     28.
#07CF   ADC  A,D            8A         .
#07D0   INC  HL             23         #
#07D1   JR   NC,#0807       3034       04
#07D3   DEC  C              0D         .
#07D4   SBC  A,L            9D         .
#07D5   LD   E,D            5A         Z
#07D6   INC  L              2C         ,
#07D7   LD   D,(HL)         56         V
#07D8   LD   (HL),#31       3631       61
#07DA   LD   (#0D38),A      32380D     28.
#07DD   SBC  A,L            9D         .
#07DE   LD   D,(HL)         56         V
#07DF   INC  (HL)           34         4
#07E0   LD   (HL),#34       3634       64
#07E2   DEC  C              0D         .
#07E3   RST  38H            FF         .
#07E4   DEC  C              0D         .
#07E5   LD   D,(HL)         56         V
#07E6   LD   (HL),#31       3631       61
#07E8   LD   (#0D38),A      32380D     28.
#07EB   ADD  A,B            80         .
#07EC   LD   B,C            41         A
#07ED   INC  L              2C         ,
#07EE   INC  HL             23         #
#07EF   LD   B,E            43         C
#07F0   INC  (HL)           34         4
#07F1   DEC  C              0D         .
#07F2   SBC  A,(HL)         9E         .
#07F3   LD   B,E            43         C
#07F4   LD   C,A            4F         O
#07F5   LD   C,L            4D         M
#07F6   LD   C,L            4D         M
#07F7   LD   D,L            55         U
#07F8   LD   D,H            54         T
#07F9   DEC  C              0D         .
#07FA   ADD  A,B            80         .
#07FB   LD   B,H            44         D
#07FC   INC  L              2C         ,
#07FD   LD   SP,#0D33       31330D     13.
